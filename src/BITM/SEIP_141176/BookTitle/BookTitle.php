<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
if(!isset($_SESSION))session_start();
//use PDO;

class BookTitle extends DB
{
    public $id;
    public $author_name;
    public $booktitle;

    public function __construct()
    {
        parent::__construct();

    }

    public function setData($postVariabledata = NULL)
    {
        if (array_key_exists("id", $postVariabledata)) {
            $this->id = $postVariabledata['id'];

        }
        if (array_key_exists("booktitle", $postVariabledata)) {
            $this->booktitle = $postVariabledata['booktitle'];
        }
        if (array_key_exists("author_name", $postVariabledata)) {
            $this->author_name = $postVariabledata['author_name'];
        }

    }
    public function store(){
        $arrData=array($this->booktitle,$this->author_name);
        $sql="insert into book_title(booktitle,author_name)VALUES
            (?,?)";
       $STH= $this->DBH->prepare($sql);
       $result= $STH->execute($arrData);
        if($result==NULL)
            Message::message("Failed! DATA HAS not BEEN INSERTED SUCCESSFULLY");
        else
            Message::message("Success! DATA HAS BEEN INSERTED SUCCESSFULLY");
        Utility::redirect('create.php');
    }
}




