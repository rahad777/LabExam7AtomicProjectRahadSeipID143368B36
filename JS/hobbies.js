$(document).ready(function() {

    // Generate a simple captcha
    $('#hobbieform').bootstrapValidator({

        message: 'This value is not valid',

        fields: {
            username: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required and can\'t be empty'
                    }
                }
            },
            hobbies: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The hobbies is required and can\'t be empty'
                    }
                }
            }
        }
    });
});